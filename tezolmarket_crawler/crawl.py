import asyncio
import aiohttp

from abstract_crawl.crawl import CrawlerAbstract
from typing import Dict, NoReturn
from .storage import MongoStorage


class Crawler(CrawlerAbstract):
    def __init__(self):
        self.url = 'https://www.tezolmarket.com/Home/GetProductQueryResult'

    async def downloader(self, params: dict, *args, **kwargs) -> Dict:
        async with aiohttp.ClientSession() as session:
            async with session.request(method='POST', url=self.url, params=params) as response:
                if response.status == 200:
                    result = await response.json()  # JSON Response Content
                elif response.status == 403:
                    raise ValueError('Error! Please check your request headers')
                else:
                    raise ValueError('Website is down Or you are offline')

        return result

    async def parser(self, product: dict, *args, **kwargs) -> Dict:
        data = {
            'ProductId': product['ProductId'],
            'FullName': product['FullName'],
            'IsAvailable': product['IsAvailable'],
            'UnitPrice': product['UnitPrice'],
            'FinalUnitPrice': product['FinalUnitPrice'],
            'StoreDiscount': product['StoreDiscount'],
        }

        return data

    async def run(self) -> NoReturn:
        categories = [
            {'CategoryId': 1},
            {'CategoryId': 24},
            {'CategoryId': 41},
            {'CategoryId': 55},
            {'CategoryId': 66},
        ]

        # Download information
        results = await asyncio.gather(
            *[self.downloader(params=params) for params in categories])

        # Pars information
        data = []
        for result in results:
            data += await asyncio.gather(
                *[self.parser(product=product) for product in result['Products']])

        # Store parsed data in mongoDB
        mongo_storage = MongoStorage()
        await mongo_storage.store(data=data, collection='tezolmarket.com')
