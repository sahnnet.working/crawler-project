import asyncio
import platform
from tezolmarket_crawler.crawl import Crawler as tzm_crawler
from okala_crawler.crawl import Crawler as ok_crawler
from digikala_crawler.crawl import Crawler as digikala_crawler


async def main() -> None:
    tzm_task = asyncio.create_task(tzm_crawler().run())
    ok_task = asyncio.create_task(ok_crawler().run())
    digikala_task = asyncio.create_task(digikala_crawler().run())

    await tzm_task
    await ok_task
    await digikala_task


if __name__ == '__main__':
    if platform.system() == 'Windows':
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
