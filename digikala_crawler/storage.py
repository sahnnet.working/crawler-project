from Databases.mongo import MongoDatabase
from abstract_crawl.storage import StorageAbstract


class MongoStorage(StorageAbstract):

    def __init__(self):
        self.mongo = MongoDatabase()

    async def store(self, data: list | dict, collection: str, *args) -> None:
        collection = self.mongo.database[f'{collection}']
        if isinstance(data, list) and len(data) > 1:
            collection.insert_many(data)
        else:
            collection.insert_one(data)
