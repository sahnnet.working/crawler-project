import asyncio
import aiohttp

from abstract_crawl.crawl import CrawlerAbstract
from typing import NoReturn, Dict, List
from .storage import MongoStorage


class Crawler(CrawlerAbstract):

    def __init__(self):
        self.base_url = 'https://api.digikala.com'

    async def make_all_url(self, categorize: str, min_page=1, max_page=2) -> List[str]:
        list_urls = [f'/v1/categories/{categorize}/search/?page={i}' for i in
                     range(min_page, max_page)]

        return list_urls

    async def downloader(self, url: str, *args, **kwargs) -> Dict:
        async with aiohttp.ClientSession(base_url=self.base_url) as session:
            async with session.request(method='GET', url=url) as response:
                if response.status == 200:
                    result = await response.json()  # JSON Response Content
                elif response.status == 403:
                    raise ValueError('Error! Please check your request headers')
                else:
                    raise ValueError('Website is down Or you are offline')

        return result

    async def parser(self, product: dict, *args, **kwargs) -> Dict:
        id = product['id']
        title_fa = product['title_fa']
        category = product['data_layer']['category']
        category = category[4:-1]
        exist = len(product['default_variant']) != 0
        if exist:
            rrp_price = product['default_variant']['price']['rrp_price']
            selling_price = product['default_variant']['price']['selling_price']
        else:
            rrp_price = None
            selling_price = None

        data = {
            'id': id,
            'title': title_fa,
            'category': category,
            'exist': exist,
            'rrp_price': rrp_price,
            'selling_price': selling_price,
        }

        return data

    async def run(self) -> NoReturn:
        information = [
            {
                'categorize': 'dairy',
                'max_page': 30,
            },
            {
                'categorize': 'protein-foods',
                'max_page': 19,
            },
            {
                'categorize': 'groceries',
                'max_page': 95,
            },
        ]

        list_urls = await asyncio.gather(
            *[self.make_all_url(categorize=info['categorize'], max_page=info['max_page']) for info in information])

        # Download information
        results = []
        for list_url in list_urls:
            results += await asyncio.gather(
                *[self.downloader(url=url) for url in list_url])

        # Pars information
        data = []
        for result in results:
            data += await asyncio.gather(
                *[self.parser(product=product) for product in result['data']['products']])

        # Store parsed data in mongoDB
        mongo_storage = MongoStorage()
        await mongo_storage.store(data=data, collection='digikala.com')
