import asyncio
import aiohttp

from abstract_crawl.crawl import CrawlerAbstract
from typing import NoReturn, Dict, List
from .storage import MongoStorage


class Crawler(CrawlerAbstract):
    def __init__(self):
        self.url = 'https://apigateway.okala.com/api/Search/v1/Product/Search?categorySlugs='

    def __get_update_products(self, products: list) -> List:
        update_products = {}
        for product in products:
            try:
                up_product = update_products[product['id']]
                if product['price'] > up_product['price']:
                    update_products[product['id']] = product
            except:
                update_products[product['id']] = product

        return [v for v in update_products.values()]

    async def downloader(self, slug: str, *args, **kwargs) -> Dict:
        async with aiohttp.ClientSession() as session:
            async with session.request(method='GET', url=f'{self.url}{slug}') as response:
                if response.status == 200:
                    result = await response.json()  # JSON Response Content
                elif response.status == 403:
                    raise ValueError('Error! Please check your request headers')
                else:
                    raise ValueError('Website is down Or you are offline')

        return result

    async def parser(self, product: dict, *args, **kwargs) -> Dict:
        data = {
            'id': product['id'],
            'name': product['name'],
            'description': product['description'],
            'exist': product['exist'],
            'price': product['price'],
            'okPrice': product['okPrice'],
        }

        return data

    async def run(self) -> NoReturn:
        categorySlugs = [
            'dairy-products',
            'trailmmix-driedfruit',
            'proteins',
            'macaroni-noodle',
            'legume',
            'stationery',
        ]

        # Download information
        results = await asyncio.gather(
            *[self.downloader(slug=slug) for slug in categorySlugs])

        # Pars information
        data = []
        for result in results:
            information = await asyncio.gather(
                *[self.parser(product=product) for product in result['entities']])

            data += self.__get_update_products(products=information)

        # Store parsed data in mongoDB
        mongo_storage = MongoStorage()
        await mongo_storage.store(data=data, collection='okala.com')
